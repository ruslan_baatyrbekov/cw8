<?php

namespace App\Http\Controllers;

use App\Http\Requests\SessionRequest;
use App\Models\User;
use Illuminate\Http\Request;

class SessionsController extends AuthController
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse
     */
    public function create(Request $request)
    {
        if ($request->session()->exists('ticket')) {
            return redirect()->route('home')->with('error', 'You are already login!');
        }
        return view('sessions.create');
    }

    /**
     * @param SessionRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(SessionRequest $request)
    {
        $user = User::where('ticket', $request->get('ticket'))->firstOrFail();
        if ($this->auth($user, $request->get('password'))) {
            $this->logIn($user);
            return redirect()->route('home')->with('success', 'You are login success');
        } else {
            return back()->with('error', 'Incorrect ticket or password');
        }
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(): \Illuminate\Http\RedirectResponse
    {
        $this->logOut();
        return redirect()->route('sessions.login')->with('success', 'You are success logout');
    }
}
