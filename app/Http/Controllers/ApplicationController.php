<?php

namespace App\Http\Controllers;

use App\Http\Requests\ApplicationRequest;
use App\Models\Book;
use Illuminate\Http\Request;

class ApplicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index(Book $book,Request $request)
    {
        $ticket = $request->session()->get('ticket');
        return view('users.application.index',compact('book','ticket'));
    }

    /**
     * @param Request $request
     */
    public function store(ApplicationRequest $request){
        $user = auth_user();
        $user->book()->attach($request->input('bookId'), ['returnDate' => $request->input('returnDate')]);
        return redirect()->route('users.myBooks.index')->with('success', 'Book successfuly recieved!');
    }
}
