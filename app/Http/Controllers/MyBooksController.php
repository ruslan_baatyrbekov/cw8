<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MyBooksController extends Controller
{
    public function index(Request $request)
    {
        $ticket = $request->session()->get('ticket');
        $user = auth_user();
        $today = date("Y-m-d");
        return view('users.myBooks.index',compact('ticket','user','today'));
    }
}
