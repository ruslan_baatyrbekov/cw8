<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegisterRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UsersController extends AuthController
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function register(Request $request)
    {
        if ($request->session()->exists('ticket')) {
            return redirect()->route('home')->with('error', 'You are already registered!');
        }
        return view('users.register');
    }

    /**
     * @param RegisterRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(RegisterRequest $request)
    {
        $payload = collect($request->all());
        $payload['password'] = Hash::make($payload->get('password'));
        $payload['ticket'] = md5(uniqid(rand(),1));
        $user = User::create($payload->all());
        $this->logIn($user);
        return redirect()->route('home')->with('success', 'Your ticket '.auth_user()->ticket.'. It must be recorded for registration!');
    }
}
