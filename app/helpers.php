<?php

function auth_user()
{

    if (session()->exists('ticket'))
    {
        return App\Models\User::find(session()->get('user_id'));
    } else {
        return NULL;
    }
}
