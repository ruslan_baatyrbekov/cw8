<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[\App\Http\Controllers\MainController::class,'index'])->name('home');
Route::get('/{book}/application',[\App\Http\Controllers\ApplicationController::class,'index'])->name('users.application.index');
Route::post('application/store', [App\Http\Controllers\ApplicationController::class, 'store'])->name('users.application.store');
Route::get('/user/myBooks',[\App\Http\Controllers\MyBooksController::class,'index'])->name('users.myBooks.index');


Route::get('/users/register', [App\Http\Controllers\UsersController::class, 'register'])->name('users.register');
Route::post('/users', [App\Http\Controllers\UsersController::class, 'store'])->name('users.store');

Route::get('/login', [App\Http\Controllers\SessionsController::class, 'create'])->name('sessions.login');
Route::post('/login', [App\Http\Controllers\SessionsController::class, 'store'])->name('sessions.store');
Route::delete('/logout', [App\Http\Controllers\SessionsController::class, 'destroy'])->name('sessions.delete');
