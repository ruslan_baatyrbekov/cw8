<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="https://kit.fontawesome.com/d9e95b620e.js" crossorigin="anonymous"></script>
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
<div id="app">
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-dark bg-dark shadow-sm">
            <H1><a href="{{route('home')}}">Library</a></H1>
            <hr>
            @if (auth_user())
                <div class="login">
                    <a href="{{route('users.myBooks.index')}}">My Books</a> |
                    <h4>{{auth_user()->name}}</h4>
                    <form action="{{route('sessions.delete')}}" method="post">
                        @csrf
                        @method('delete')
                        <button type="submit" class="btn btn-sm btn-outline-danger">Log out</button>
                    </form>
                </div>
            @else
                <div class="login">
                    <a href="{{route('sessions.login')}}">Log In</a>  |
                    <a href="{{route('users.register')}}">Register</a>
                </div>
            @endif
        </nav>
        <main class="py-4">
            <div class="container mt-4">
                @yield('content')
            </div>
        </main>
    </div>
</div>
</body>
</html>
