@extends('layouts.main')

@section('content')
    <div class="row">
        <div class="col-6 offset-3 align-self-center">
            @include('notifications.alerts')
            <h1 class="text-center">Register</h1>
            <hr>
            <form action="{{route('users.store')}}" method="post">
                @csrf
                <div class="form-group">
                    <label for="exampleInputName1">Name</label>
                    <input type="text" class="form-control" id="exampleInputName1" name="name">
                </div>
                <div class="form-group">
                    <label for="exampleInputSurname1">Surname</label>
                    <input type="text" class="form-control" id="exampleInputSurname1" name="surname">
                </div>
                <div class="form-group">
                    <label for="exampleInputPatronymic1">Patronymic</label>
                    <input type="text" class="form-control" id="exampleInputPatronymic1" name="patronymic">
                </div>
                <div class="form-group">
                    <label for="exampleInputAdress1">Adress</label>
                    <input type="text" class="form-control" id="exampleInputAdress1" name="adress">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassport1">Passort</label>
                    <input type="text" class="form-control" id="exampleInputPassport1" name="passport">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Password</label>
                    <input type="password" class="form-control" id="exampleInputPassword1" name="password">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword2">Confirm password</label>
                    <input type="password" class="form-control" id="exampleInputPassword2" name="password_confirmation">
                </div>
                <button type="submit" class="btn btn-outline-primary">Get a library card</button>
            </form>
        </div>
    </div>
@endsection
