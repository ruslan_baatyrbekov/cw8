@extends('layouts.main')

@section('content')
    @include('notifications.alerts')
    <form action="{{route('users.application.store')}}" method="post">
        @csrf
        <div class="form-group">
            <label for="exampleInputTicket1">Ticket:</label>
            <input type="text" name="ticket" value="{{$ticket}}" readonly>
        </div>
        <div class="form-group">
            <label for="exampleInputIdAuthorName1">Book Id</label>
            <input type="text" name="bookId" value="{{$book->id}}" readonly>
            <label for="exampleInputIdAuthorName1">Author</label>
            <input type="text" name="bookAuthor" value="{{$book->author->name}}" readonly>
            <label for="exampleInputIdAuthorName1">Book</label>
            <input type="text" name="bookName" value="{{$book->name}}" readonly>
        </div>
        <div class="form-group">
            <label for="exampleInputReturnDate">Return Date</label>
            <input type="date" class="form-control" id="exampleInputPatronymic1" name="returnDate">
        </div>
        <button type="submit" class="btn btn-outline-primary">Receive</button>
    </form>
@endsection



