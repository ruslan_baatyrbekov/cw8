@extends('layouts.main')

@section('content')
    @include('notifications.alerts')
    <h1>Books by genre</h1>
    @foreach($genres as $genre)
    <div class="container">
        <a href="#demo-{{$genre->id}}" data-toggle="collapse"><span class="icon"><h1>{{$genre->name}}</h1></span></a>

        <div id="demo-{{$genre->id}}" class="collapse">
                <div class="row row-cols-lg-4 row-cols-md-4 row-cols-sm-2">
            @foreach($books as $book)
                @if($book->genre_id == $genre->id)
                    <div class="p-1">
                        <div class="card p-2 m-1 ">
                            <img src="{{asset('/storage/' . $book->picture)}}" class="card-img-top" alt="...">
                            <div class="card-body-1">
                                <p class="card-text">Book: {{$book->name}}</p>
                                <p class="card-text">Genre: {{$book->genre->name}}</p>
                                <p class="card-text">Author: {{$book->author->name}}</p>
                                <a href="{{route('users.application.index', ['book' => $book])}}" class="btn-secondary">Receive</a>
                            </div>
                        </div>
                    </div>
                @endif
            @endforeach
                    </div>
        </div>
    </div>
    @endforeach

@endsection





