@extends('layouts.main')

@section('content')
@include('notifications.alerts')
<h1>{{$user->name}} books</h1>
<h2>Ticket: {{auth_user()->ticket}}</h2>
<div class="row row-cols-lg-4 row-cols-md-4 row-cols-sm-2">
    @foreach($user->book as $book)
        <div class="p-1">
            <div class="card p-2 m-1 ">
                <img src="{{asset('/storage/' . $book->picture)}}" class="card-img-top" alt="...">
                <div class="card-body-1">
                    <p class="card-text">Book: {{$book->name}}</p>
                    <p class="card-text">Genre: {{$book->genre->name}}</p>
                    <p class="card-text">Author: {{$book->author->name}}</p>
                    @if($today < $book->pivot->returnDate)
                        <p class="card-text">Must be passed before: {{$book->pivot->returnDate}}</p>
                    @else
                        <p class="card-text badge-danger">Expired: {{$book->pivot->returnDate}}</p>
                    @endif
                </div>
            </div>
        </div>
    @endforeach
</div>
@endsection



