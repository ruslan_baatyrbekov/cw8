<?php

namespace Database\Factories;

use App\Models\Book;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class BookFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Book::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'picture' => $this->getImage(rand(1,11)),
            'description' => $this->faker->text,
            'genre_id' => rand(1,5),
            'author_id' => rand(1,5),
        ];
    }

    private function getImage($image_number = 1)
    {
        $path = storage_path() . "/seeds/books/" . $image_number . ".jpg";
        $image_name = md5($path) . '.jpg';
        $resize = Image::make($path)->fit(400)->encode('jpg');
        Storage::disk('public')->put('books/'.$image_name, $resize->__toString());
        return 'books/'.$image_name;
    }
}
