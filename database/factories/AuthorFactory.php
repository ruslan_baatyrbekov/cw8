<?php

namespace Database\Factories;

use App\Models\Author;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class AuthorFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Author::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'picture' => $this->getImage(rand(1,9)),
            'description' => $this->faker->text,
        ];
    }

    private function getImage($image_number = 1)
    {
        $path = storage_path() . "/seeds/authors/" . $image_number . ".jpg";
        $image_name = md5($path) . '.jpg';
        $resize = Image::make($path)->fit(400)->encode('jpg');
        Storage::disk('public')->put('authors/'.$image_name, $resize->__toString());
        return 'authors/'.$image_name;
    }
}
